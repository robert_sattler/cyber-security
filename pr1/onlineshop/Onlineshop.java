package pr1.onlineshop;

import java.util.ArrayList;
import java.util.Scanner;

public class Onlineshop
{

    private ArrayList<Bestellung> bestellungen;

    public Onlineshop()
    {
        bestellungen = new ArrayList<Bestellung>();
    }

    public static void main(String[] args)
    {
        Onlineshop shop = new Onlineshop();
        shop.startShop();
    }

    // Funktion für "String Pad", da Java keine eigenen mitbringt.. Füllt einen String mit einem bestimmten Zeichen (Leerzeichen) bis zur Länge n auf (Entweder rechts oder links)
    public static String padRight(String s, int n)
    {
        return String.format("%1$-" + n + "s", s);
    }

    public static String padLeft(String s, int n)
    {
        return String.format("%1$" + n + "s", s);
    }

    public void startShop()
    {
        // Produkte anlegen
        Produkt apfel = new Produkt("Apfel", 1.10);
        Produkt joghurt = new Produkt("Joghurt", 0.39);
        Produkt milch = new Produkt("Milch", 1.39);

        // Scanner anlegen sowie ArrayList für Bestellungen
        Scanner sc = new Scanner(System.in);

        // Willkommensnachricht
        System.out.println("Willkommen im ALDI Online Shop 1.0");
        System.out.println("----------------------------------\n");

        // Variable für Bestellschleife (j = weitere Bestellung, n = aufhören / Quittung)
        String weitere_bestellung = "j";

        // Schleife (do-while).. bedeutet, dass die Schleife mindestens 1x ausgeführt wird, unabhängig von der Bedingung
        do
        {
            System.out.println("Was möchten Sie bestellen?");
            System.out.println("  (1) Apfel");
            System.out.println("  (2) Joghurt");
            System.out.println("  (3) Milch\n");
            System.out.print("Ihre Wahl: ");

            int produkt_wahl = 0;

            try
            {
                produkt_wahl = Integer.parseInt(sc.nextLine());
            }
            catch (Exception e)
            {
                System.out.println("Ungültige Eingabe! Bitte geben Sie eine Zahl ein.\n");
                weitere_bestellung = "j";
                continue;
            }

            Bestellung bestellung = new Bestellung();

            switch (produkt_wahl)
            {
                case 1:
                    bestellung.setProdukt(apfel);
                    break;

                case 2:
                    bestellung.setProdukt(joghurt);
                    break;

                case 3:
                    bestellung.setProdukt(milch);
                    break;

                default:
                    System.out.println("Ungültige Wahl. Bitte eine Zahl von 1-3 eingeben!\n");
                    weitere_bestellung = "j";
                    continue;
            }

            int produkt_anzahl = 0;

            System.out.print("Geben Sie die Anzahl für '" + bestellung.getProdukt().getName() + "' ein: ");

            try
            {
                produkt_anzahl = Integer.parseInt(sc.nextLine());
            }
            catch (Exception e)
            {
                System.out.println("Ungültige Eingabe! Bitte geben Sie eine Zahl ein.\n");
                weitere_bestellung = "j";
                continue;
            }

            if (produkt_anzahl > 0)
            {
                bestellung.setAnzahl(produkt_anzahl);
            }
            else
            {
                System.out.println("Ungültige Eingabe! Die Anzahl muss mindestens 1 betragen.\n");
                weitere_bestellung = "j";
                continue;
            }

            this.bestellungen.add(bestellung);

            System.out.print("Vielen Dank! Die Bestellung (" +
                    bestellung.getProdukt().getName() + ", " +
                    bestellung.getAnzahl() +
                    "x) wurde hinzugefügt.\nWünschen Sie eine weitere Bestellung oder soll die Quittung ausgegeben werden? ('j' für weitere Bestellung)\n\nIhre Wahl: ");

            weitere_bestellung = sc.nextLine();
        }
        while (weitere_bestellung.equals("j"));

        sc.close();
        this.druckeQuittung();
    }

    public void druckeQuittung()
    {
        double gesamtpreis = 0.0;

        System.out.println("Quittung\n\n");
        System.out.println("Produkt     Anzahl     Preis einzeln   Preis Gesamt");
        System.out.println("---------------------------------------------------");

        for (Bestellung bestellung : this.bestellungen)
        {
            gesamtpreis += (bestellung.getProdukt().getPreis() * bestellung.getAnzahl());
            System.out.print(padRight(bestellung.getProdukt().getName(), 12));
            System.out.print(padLeft(Integer.toString(bestellung.getAnzahl()), 6));
            System.out.print(padLeft(String.format("%.2f", bestellung.getProdukt().getPreis()) + " €", 18));
            System.out.print(padLeft(String.format("%.2f", bestellung.getProdukt().getPreis() * bestellung.getAnzahl()) + " €", 15) + "\n");
        }

        System.out.println("---------------------------------------------------");
        System.out.println(padLeft(String.format("%.2f", gesamtpreis) + " €", 51));
    }
}