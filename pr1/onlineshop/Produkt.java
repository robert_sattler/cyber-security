package pr1.onlineshop;

public class Produkt
{
    private String name;
    private double preis;

    public Produkt(String name, double preis)
    {
        this.name = name;
        this.preis = preis;
    }

    public double getPreis()
    {
        return preis;
    }

    public void setPreis(double preis)
    {
        this.preis = preis;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}