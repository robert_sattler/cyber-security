package pr1.uebung7;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Entfernungen
{

    public static void main(String[] args)
    {
        // Beispiel
        int entfernung = berechneEntfernung("MA", "MA", "LU", "HD", "Kaiserslautern", "KA");
        System.out.println("Die Entfernung beträgt " + entfernung + " km.");
    }

    /**
     * Gibt die addierte Entfernung zurück für eine beliebige Anzahl an Städten
     *
     * @param staedte Beliebig viele Städte als Parameter aneinander gereiht, zwischen denen die Gesamtentfernung gemessen werden soll (Kennzeichenkürzel oder voller Name der Stadt)
     *
     * @return Entfernung als Integer
     */
    public static int berechneEntfernung(String... staedte)
    {
        ArrayList<ArrayList<Object>> entfernungen = baueEntfernungsListe();

        // Fehlerwert, falls nicht mindestens eine Stadt angegeben wurde.
        int entfernung = -1;

        // Mindestens 2 Städte angegeben..
        if (staedte.length >= 2)
        {
            // Entfernung auf 0 gesetzt zur Initialisierung
            entfernung = 0;

            // Schleife läuft von Null bis Anzahl der Städte - 1 (Beispiel bei 3 Städten: Entfernung zwischen Stadt 1 und 2, und von Stadt 2 zu 3, also 2 Schleifendurchläufe)
            for (int i = 0; i < (staedte.length - 1); i++)
            {
                // Gehe die Entfernungsliste durch
                for (ArrayList<Object> zeile : entfernungen)
                {

                    String vonStadtGesucht = staedte[i];
                    String zuStadtgesucht = staedte[i + 1];
                    String vonStadtName = (String) zeile.get(0);
                    String vonStadtKuerzel = (String) zeile.get(1);
                    String zuStadtName = (String) zeile.get(2);
                    String zuStadtKuerzel = (String) zeile.get(3);

                    int entfernungZwischenDenStaedten = (int) zeile.get(4);

                    // Wenn die übergebene erste Stadt mit dem Kürzel oder Städtenamen übereinstimmt und die zweite Stadt ebenfalls, dann addiere die Entfernung dazu.
                    if ((vonStadtName.equals(vonStadtGesucht) || vonStadtKuerzel.equals(vonStadtGesucht))
                            && (zuStadtName.equals(zuStadtgesucht) || zuStadtKuerzel.equals(zuStadtgesucht)))
                    {
                        entfernung = entfernung + entfernungZwischenDenStaedten;
                    }
                }
            }
        }

        return entfernung;
    }

    /**
     * Baut eine Liste auf von Entfernungen zwischen den Städten einer übergebenen ArrayList (die diese Daten enthält).
     *
     * @return Eine Liste mit Entfernungen, jede Zeile hat den Aufbau:
     * index 0: von Stadtname ausgeschrieben
     * index 1: von Stadtname (Kürzel)
     * index 2: zu Stadtname ausgeschrieben
     * index 3: zu Stadtname (Kürzel)
     * index 4: Entfernung zwischen den beiden Städten
     */
    public static ArrayList<ArrayList<Object>> baueEntfernungsListe()
    {
        // CSV-Datei einlesen
        ArrayList<String[]> dateiTabelle = csvEinlesenArrayList("./pr1/resources/entfernungen.csv", ",");

        // Rückgabe-ArrayList anlege
        ArrayList<ArrayList<Object>> entfernungen = new ArrayList<ArrayList<Object>>();

        // Hilfs-ArrayList, in der die Paare aus Städtenamen und Kennzeichenkürzel stehen
        ArrayList<String[]> staedteNamenUndKuerzel = new ArrayList<String[]>();

        // Befüllung der Städtenamen / Kürzel ArrayList
        for (int i = 0; i < dateiTabelle.get(0).length; i++)
        {
            String[] stadtArray = new String[2];

            stadtArray[0] = dateiTabelle.get(0)[i];
            stadtArray[1] = dateiTabelle.get(1)[i];

            staedteNamenUndKuerzel.add(stadtArray);
        }

        // Befüllung der Entfernungstabelle (starte bei Zeilenindex 2 (Zeile 3)
        for (int i = 2; i < dateiTabelle.size(); i++)
        {
            // Gehe durch die Spalten
            for (int k = 0; k < staedteNamenUndKuerzel.size(); k++)
            {
                // Anlegen eines neuen Eintrags für die Entfernungszeile.
                // Der Aufbau des Eintrags ist wie folgt:
                // 	- index 0: von Stadtname ausgeschrieben
                //  - index 1: von Stadtname (Kürzel)
                //  - index 2: zu Stadtname ausgeschrieben
                //  - index 3: zu Stadtname (Kürzel)
                //  - index 4: Entfernung zwischen den beiden Städten

                ArrayList<Object> entfernungsZeile = new ArrayList<Object>();

                // Wenn Spaltenindex = Zeilenindex (beduetet Entfernung von einer Stadt zur Gleichen, demnach Entfernung 0)
                if ((i - 2) == k)
                {
                    entfernungsZeile.add(staedteNamenUndKuerzel.get(k)[0]);
                    entfernungsZeile.add(staedteNamenUndKuerzel.get(k)[1]);
                    entfernungsZeile.add(staedteNamenUndKuerzel.get(k)[0]);
                    entfernungsZeile.add(staedteNamenUndKuerzel.get(k)[1]);
                    entfernungsZeile.add(0);
                    entfernungen.add(entfernungsZeile);
                }
                // Wenn Spaltenindex != Zeilenindex (Wenn es verschiedene Städte sind..)
                else
                {
                    // Von- und Zu Stadt (Name + Kürzel) hinzufügen
                    entfernungsZeile.add(staedteNamenUndKuerzel.get(i - 2)[0]);
                    entfernungsZeile.add(staedteNamenUndKuerzel.get(i - 2)[1]);
                    entfernungsZeile.add(staedteNamenUndKuerzel.get(k)[0]);
                    entfernungsZeile.add(staedteNamenUndKuerzel.get(k)[1]);

                    int entfernung = 0;

                    // Wenn in der entsprechenden Zelle kein "-" steht, also eine Zahl.. weise diese Zahl "entfernung" zu
                    if (!dateiTabelle.get(i)[k].equals("-"))
                    {
                        entfernung = Integer.parseInt(dateiTabelle.get(i)[k]);
                    }
                    // Ansonsten, schaue ob in der Zelle, wenn man Spalte und Zeile tauscht (Spiegelzelle) ein Wert steht
                    // und weise diesen "entfernung" zu
                    else if (!dateiTabelle.get(k + 2)[i - 2].equals("-"))
                    {
                        entfernung = Integer.parseInt(dateiTabelle.get(k + 2)[i - 2]);
                    }

                    // Füge den Entfernungswert hinzu und anschließend die komplette Zeile unserer Entfernungstabelle
                    entfernungsZeile.add(entfernung);
                    entfernungen.add(entfernungsZeile);
                }
            }
        }

        return entfernungen;
    }

    /**
     * CSV Datei einlesen in ArrayList aus String-Arrays (Tabelle)
     *
     * @param path      Dateipfad
     * @param delimiter Spaltentrennzeichen der CSV-Datei
     *
     * @return Die CSV-Datei als ArrayList<String[]>
     */
    public static ArrayList<String[]> csvEinlesenArrayList(String path, String delimiter)
    {
        ArrayList<String[]> tabelle = new ArrayList<String[]>();

        try
        {
            Scanner sc = new Scanner(new File(path));
            sc.nextLine();

            while (sc.hasNextLine())
            {
                String line = sc.nextLine();

                String[] spalten = line.split(delimiter);

                if (spalten.length > 1 && spalten[0] != "")
                {
                    tabelle.add(spalten);
                }
            }

            sc.close();
        }
        catch (FileNotFoundException e)
        {
            System.out.println("Die angegebene Datei wurde nicht gefunden.");
        }

        return tabelle;
    }
}
