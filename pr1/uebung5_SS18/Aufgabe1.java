package pr1.uebung5_SS18;

import java.util.Scanner;

public class Aufgabe1
{

    public static void main(String[] args)
    {
        // b)
        int[] testArray = leseSechsGanzzahlen();
        System.out.println("Produkt aus Array: " + produktAusArrayElementen(testArray));

        // c)
        if (testeVierArrays())
        {
            System.out.println("Die Methode produktAusArrayElementen() wurde für 4 Arrays erfolgreich getestet.");
        }
        else
        {
            System.out.println("Die Methode produktAusArrayElementen() wurde für 4 Arrays NICHT erfolgreich getestet.");
        }

        // d)
        for (int zahl : testArray)
        {
            if (istPrimzahl(zahl))
            {
                System.out.println(zahl + " IST eine Primzahl.");
            }
            else
            {
                System.out.println(zahl + " ist KEINE Primzahl.");
            }
        }
    }

    // Aufgabe a)
    public static int[] leseSechsGanzzahlen()
    {
        Scanner sc = new Scanner(System.in);
        int[] zahlenArray = new int[6];

        for (int i = 0; i < 6; i++)
        {
            System.out.print("Bitte geben Sie die " + (i + 1) + ". Zahl ein: ");
            int zahl = Integer.parseInt(sc.nextLine());
            zahlenArray[i] = zahl;
        }

        sc.close();
        return zahlenArray;
    }

    // Aufgabe b)
    public static long produktAusArrayElementen(int[] zahlenArray)
    {
        long produkt = zahlenArray[0];

        for (int i = 1; i < zahlenArray.length; i++)
        {
            produkt = produkt * zahlenArray[i];
        }

        return produkt;
    }

    // c)
    public static boolean testeVierArrays()
    {
        boolean testErfolgreich = true;

        int[] array1 = {1, 3, 5}; // 1*3*5 = 15
        int[] array2 = {2, 2, 2, 2, 2}; // 2^5 = 32
        int[] array3 = {5, 4, 3, 2, 1, -1}; // -120
        int[] array4 = {10, 120, 2, 13}; // 31200

        if (produktAusArrayElementen(array1) != 15)
        {
            testErfolgreich = false;
        }

        if (produktAusArrayElementen(array2) != 32)
        {
            testErfolgreich = false;
        }

        if (produktAusArrayElementen(array3) != -120)
        {
            testErfolgreich = false;
        }

        if (produktAusArrayElementen(array4) != 31200)
        {
            testErfolgreich = false;
        }

        return testErfolgreich;
    }

    // d)
    public static boolean istPrimzahl(int n)
    {
        // 2 ist die kleinste Primzahl
        if (n >= 2)
        {
            // Schaue ob eine Zahl zwischen 2 und n ein Teiler von n ist (1 müssen wir nicht überprüfen, da 1 immer Teiler ist).
            // Das gleiche gilt für n selbst, da n immer Teiler n
            // Falls ein Teiler gefunden wurde, ist die Zahl keine Primzahl

            for (int i = 2; i < n; i++)
            {
                if (n % i == 0)
                {
                    return false;
                }
            }

            return true;
        }
        else
        {
            return false;
        }
    }
}
