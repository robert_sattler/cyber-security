package pr1.vorlesung11;

import java.util.Scanner;

public class Taschenrechner
{

    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("Geben Sie die 1. Zahl ein: ");
        double ersteZahl = Double.parseDouble(sc.nextLine());

        System.out.print("Geben Sie die 2. Zahl ein: ");
        double zweiteZahl = Double.parseDouble(sc.nextLine());

        System.out.print("Geben Sie die Rechnoperation an (+,-,*,/): ");
        char operator = sc.nextLine().charAt(0);

        System.out.println("Das Ergebnis ist: " + rechne(ersteZahl, zweiteZahl, operator));

        sc.close();
    }

    public static double rechne(double zahl1, double zahl2, char operator)
    {
        double ergebnis = 0.0;

        switch (operator)
        {
            case '+':
                ergebnis = zahl1 + zahl2;
                break;

            case '-':
                ergebnis = zahl1 - zahl2;
                break;

            case '*':
                ergebnis = zahl1 * zahl2;
                break;

            case '/':
                ergebnis = zahl1 / zahl2;
                break;
        }

        return ergebnis;
    }
}
