package pr1.uebung8;

import java.util.ArrayList;
import java.util.Scanner;

public class AktienUI
{
    private static Scanner sc;

    public static void main(String[] args)
    {
        sc = new Scanner(System.in);

        System.out.println("Aktienkurs-Abfrage 1.0");
        System.out.println("----------------------\nWas möchten Sie tun?\n\n");

        boolean beenden = false;

        while (!beenden)
        {
            System.out.println("(1) Aktienkurs für Firma abfragen");
            System.out.println("(2) Beenden");
            System.out.print("Ihre Wahl: ");
            int auswahl = Integer.parseInt(sc.nextLine());

            switch (auswahl)
            {
                case 1:
                    sucheAktienKurs();
                    break;
                case 2:
                    beenden = true;
                    break;
            }
        }

        System.out.println("Auf wiedersehen!");
        sc.close();
        System.exit(0);
    }

    public static void sucheAktienKurs()
    {
        System.out.print("Geben Sie einen Firmennamen ein: ");
        String name = sc.nextLine();

        ArrayList<String[]> firmen = AktienUebung.getCompanyIds(name);

        int counter = 1;

        for (String[] firma : firmen)
        {
            System.out.println("(" + (counter) + ") " + firma[1] + " - " + firma[0]);
            counter++;
        }

        System.out.print("Ihre Wahl: ");
        int auswahl = Integer.parseInt(sc.nextLine());

        String id = firmen.get(auswahl - 1)[0];
        String nameCompany = firmen.get(auswahl - 1)[1];

        double kurs = AktienUebung.getAktienkurs(id);

        System.out.println("Der Aktienkurs von " + nameCompany + " beträgt: " + kurs);
    }
}