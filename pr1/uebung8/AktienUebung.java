package pr1.uebung8;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AktienUebung
{
    public static void main(String[] args)
    {
//		System.out.println(getAktienkurs("GS"));
//		
//		ArrayList<String[]> ids = getCompanyIds("Apple");
//		for (String[] id : ids)
//		{
//			System.out.println(id[0]);
//		}

//		ArrayList<String> idsDow30 = getIdsDow30();
//		for (String id : idsDow30)
//		{
//			System.out.println(id);
//		}

        System.out.println(calcDowJonesIndex());
        //getIdsDow30_regex();
    }

    // Aufgabe 1 Vorbereitung 1
    public static double getAktienkurs(String companyId)
    {
        URL url = null;

        try
        {
            url = new URL("https://api.iextrading.com/1.0/stock/" + companyId.toLowerCase() + "/quote?format=csv");
        }
        catch (MalformedURLException e)
        {
            System.out.println("Die URL konnte nicht aufgerufen werden.");
        }

        ArrayList<String[]> aktie = csvEinlesenArrayList(url, ",");

        double kurs = Double.parseDouble(aktie.get(0)[7]);

        return kurs;
    }

    public static ArrayList<String[]> getCompanyIds(String name)
    {
        ArrayList<String[]> idList = new ArrayList<String[]>();

        URL url = null;

        try
        {
            url = new URL("https://api.iextrading.com/1.0/ref-data/symbols?format=csv");
        }
        catch (MalformedURLException e)
        {
            System.out.println("Die URL konnte nicht aufgerufen werden.");
        }

        ArrayList<String[]> companyList = csvEinlesenArrayList(url, ",");

        for (String[] line : companyList)
        {

            String id = line[0];
            String companyName = line[1];

            if (companyName.toLowerCase().contains(name.toLowerCase()))
            {
                String[] company = new String[2];

                company[0] = id;
                company[1] = companyName;

                idList.add(company);
            }
        }

        return idList;
    }

    public static double calcDowJonesIndex()
    {
        double index = 0.0;

        ArrayList<String> idsDow30 = getIdsDow30_regex();
        for (String id : idsDow30)
        {
            double kurs = getAktienkurs(id);
            index += kurs;
        }

        index /= 0.14748071991788;

        return index;
    }

    public static ArrayList<String> getIdsDow30()
    {
        ArrayList<String> idList = new ArrayList<String>();

        Document doc = null;

        try
        {
            doc = Jsoup.connect("https://money.cnn.com/data/dow30/").get();
        }
        catch (IOException e)
        {
            System.out.println("Die URL konnte nicht aufgerufen werden.");
        }

        Elements tags = doc.select("tr td a.wsod_symbol");

        for (Element tag : tags)
        {
            idList.add(tag.text());
        }

        return idList;
    }

    public static ArrayList<String> getIdsDow30_regex()
    {
        ArrayList<String> idList = new ArrayList<String>();

        Scanner sc = null;

        URL url = null;

        try
        {
            url = new URL("https://money.cnn.com/data/dow30/");
        }
        catch (MalformedURLException e)
        {
            System.out.println("Die URL konnte nicht aufgerufen werden.");
        }

        try
        {
            sc = new Scanner(url.openStream());
        }
        catch (IOException e)
        {
            System.out.println("Die URL konnte nicht aufgerufen werden.");
        }

        sc.nextLine();

        String website_source = "";

        while (sc.hasNextLine())
        {

            String line = sc.nextLine();
            website_source += "\n" + line;
        }

        sc.close();

        Pattern pattern = Pattern.compile("<a href=\\\".*\\\" class=\\\"wsod_symbol\\\">(\\w+)<\\/a>");

        Matcher matcher = pattern.matcher(website_source);

        while (matcher.find())
        {
            idList.add(matcher.group(1));
        }

        return idList;
    }

    public static ArrayList<String[]> csvEinlesenArrayList(URL url, String delimiter)
    {

        ArrayList<String[]> tabelle = new ArrayList<String[]>();
        Scanner sc = null;

        try
        {
            sc = new Scanner(url.openStream());
        }
        catch (IOException e)
        {
            System.out.println("Die URL konnte nicht aufgerufen werden.");
        }

        sc.nextLine();

        while (sc.hasNextLine())
        {
            String line = sc.nextLine();
            String[] spalten = line.split(delimiter);
            tabelle.add(spalten);
        }

        sc.close();

        return tabelle;
    }
}