package pr1.uebung8;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AktienUebungTest
{

    @Test
    void testGetAktienkurs()
    {
        assertEquals(109.68, AktienUebung.getAktienkurs("AXP"), 0.1);
        assertEquals(183.89, AktienUebung.getAktienkurs("MCD"), 0.1);
    }
}