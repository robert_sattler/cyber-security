package pr1.vorlesung7;

public class BussgeldRechner
{

    public static void main(String[] args)
    {
        // Beispiel
        ermittleBussgeld(42, 50);
    }

    public static void ermittleBussgeld(int gefahreneGeschwindigkeit, int zulaessigeGeschwindigkeit)
    {
        int bussgeldInnerorts = 0;
        int bussgeldAusserorts = 0;

        int ueberschreitung = gefahreneGeschwindigkeit - zulaessigeGeschwindigkeit;

        // Aufbau der Elemente (Array): Index 0 = vMin, Index 1 = vMax, index 2 = Betrag innerorts, index 3 = Betrag außerorts
//		ArrayList<Integer[]> bussgeldTabelle = new ArrayList<Integer[]>();
//		
//		bussgeldTabelle.add(new Integer[]{1, 10, 15, 10});
//		bussgeldTabelle.add(new Integer[]{11, 15, 25, 20});
//		bussgeldTabelle.add(new Integer[]{16, 20, 35, 30});
//		bussgeldTabelle.add(new Integer[]{21, 25, 80, 70});
//		bussgeldTabelle.add(new Integer[]{26, 30, 100, 80});
//		bussgeldTabelle.add(new Integer[]{31, 40, 160, 120});
//		bussgeldTabelle.add(new Integer[]{41, 50, 200, 160});
//		bussgeldTabelle.add(new Integer[]{51, 60, 280, 240});
//		bussgeldTabelle.add(new Integer[]{61, 70, 480, 440});
//		bussgeldTabelle.add(new Integer[]{71, Integer.MAX_VALUE, 680, 600});

        int[][] bussgeldTabelle = new int[10][4];

        bussgeldTabelle[0] = new int[]{1, 10, 15, 10};
        bussgeldTabelle[1] = new int[]{11, 15, 25, 20};
        bussgeldTabelle[2] = new int[]{16, 20, 35, 30};
        bussgeldTabelle[3] = new int[]{21, 25, 80, 70};
        bussgeldTabelle[4] = new int[]{26, 30, 100, 80};
        bussgeldTabelle[5] = new int[]{31, 40, 160, 120};
        bussgeldTabelle[6] = new int[]{41, 50, 200, 160};
        bussgeldTabelle[7] = new int[]{51, 60, 280, 240};
        bussgeldTabelle[8] = new int[]{61, 70, 480, 440};
        bussgeldTabelle[9] = new int[]{71, Integer.MAX_VALUE, 680, 600};

        // Schleife durch alle Bußgeldtabelleneinträge
        for (int[] zeile : bussgeldTabelle)
        {
            // Arrayelemente einzeln als Variable
            int vMin = zeile[0];
            int vMax = zeile[1];
            int betragInnerorts = zeile[2];
            int betragAusserorts = zeile[3];

            // Geschwindigkeitüberschreitung innerhalb eines bestimmten Intervalls
            if (ueberschreitung >= vMin && ueberschreitung <= vMax)
            {
                bussgeldInnerorts = betragInnerorts;
                bussgeldAusserorts = betragAusserorts;
            }
        }

        System.out.println("Ihr Bußgeld beträgt " + bussgeldInnerorts + " € (innerorts) bzw. " + bussgeldAusserorts + " € (außerorts).");
    }
}
