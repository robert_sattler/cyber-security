package pr1.vorlesung14;

public class BinarySearch
{
    public static void main(String[] args)
    {
        //int[] zahlen = {3, 7, 2, 4, 1, 6, 8, 5};
        int[] zahlen = {1, 2, 3, 4, 5, 6, 7, 8};
        System.out.println(suche(zahlen, -3));
    }

    public static int suche(int[] sortiertesArray, int gesucht)
    {
        int start = 0;
        int ende = sortiertesArray.length;
        int mitte = -1;

        do
        {
            mitte = (ende + start) / 2;

            if (gesucht > sortiertesArray[mitte])
            {
                start = mitte + 1;

                if (start > sortiertesArray.length - 1)
                {
                    mitte = -1;
                    break;
                }
            }
            else
            {
                ende = mitte - 1;
                if (ende < 0)
                {
                    mitte = -1;
                    break;
                }
            }
        }
        while (gesucht != sortiertesArray[mitte]);

        return mitte;
    }
}
