package pr1.uebung10;

import java.util.ArrayList;

// Aufgabe 3

public class HSVerwaltung_App
{

    public static void main(String[] args)
    {
        Hochschulverwaltung hsvw = new Hochschulverwaltung();

        hsvw.addStudent("Mustermann", "Max", 123, "Informatik, B.Sc.");
        hsvw.addStudent("Bauer", "Regina", 345, "Germanistik, B.Sc.");
        hsvw.addStudent("Wolf", "Sebastian", 678, "Maschinenbau, B.Sc.");
        hsvw.addStudent("Mustermann", "Martina", 912, "Psychologie, B.Sc.");

        hsvw.addProfessor("Hummel", "Oliver", "Informatik", "A007a");
        hsvw.addProfessor("Paulus", "Sachar", "Informatik", "A106a");

        ArrayList<Person> gefundenePersonen = hsvw.suchePerson("m");

        for (Person person : gefundenePersonen)
        {
            System.out.println(hsvw.personToString(person));
        }
    }
}
