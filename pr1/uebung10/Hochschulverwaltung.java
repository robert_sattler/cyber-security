package pr1.uebung10;

import java.util.ArrayList;

// Aufgabe 3

public class Hochschulverwaltung
{
    private ArrayList<Student> studenten;
    private ArrayList<Professor> professoren;

    public Hochschulverwaltung()
    {
        this.studenten = new ArrayList<>();
        this.professoren = new ArrayList<>();
    }

    public void addPerson(Person person)
    {
        if (person instanceof Student)
        {
            this.studenten.add((Student) person);
        }
        else if (person instanceof Professor)
        {
            this.professoren.add((Professor) person);
        }
    }

    public void removePerson(Person person)
    {
        if (person instanceof Student)
        {
            this.studenten.remove(person);
        }
        else if (person instanceof Professor)
        {
            this.professoren.remove(person);
        }
    }

    public void addStudent(String name, String vorname, int matrikelnummer, String studiengang)
    {
        this.studenten.add(new Student(name, vorname, matrikelnummer, studiengang));
    }

    public void addProfessor(String name, String vorname, String fachgebiet, String buero)
    {
        this.professoren.add(new Professor(name, vorname, fachgebiet, buero));
    }

    public ArrayList<Person> suchePerson(String name)
    {
        ArrayList<Person> allePersonen = new ArrayList<>();
        allePersonen.addAll(this.studenten);
        allePersonen.addAll(this.professoren);

        ArrayList<Person> gefundenePersonen = new ArrayList<>();

        for (Person person : allePersonen)
        {
            if (person.getName().toLowerCase().indexOf(name.toLowerCase()) != -1 || person.getVorname().toLowerCase().indexOf(name.toLowerCase()) != -1)
            {
                gefundenePersonen.add(person);
            }
        }

        return gefundenePersonen;
    }

    public String personToString(Person person)
    {
        String personString = null;

        if (person instanceof Student)
        {
            Student student = (Student) person;
            personString = "Rolle: Student, Name: " + student.getName() + ", Vorname: " + student.getVorname() +
                    ", Matrikelnummer: " + student.getMatrikelnummer() + ", Studiengang: " + student.getStudiengang();
        }
        else if (person instanceof Professor)
        {
            Professor prof = (Professor) person;
            personString = "Rolle: Professor, Name: " + prof.getName() + ", Vorname: " + prof.getVorname() +
                    ", Büro: " + prof.getBuero() + ", Fachgebeit: " + prof.getFachgebiet();
        }

        return personString;
    }
}
