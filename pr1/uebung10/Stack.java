package pr1.uebung10;

// Aufgabe 2

public class Stack
{
    private int size;
    private int[] stackArray;
    private int indexLastElement;

    public Stack(int size)
    {
        this.size = size;
        this.stackArray = new int[size];
        this.indexLastElement = -1;
    }

    public void push(int zahl) throws RuntimeException
    {
        if (this.countItemsOnStack() >= this.size)
        {
            throw new RuntimeException("Stack ist bereits voll! (Maximal " + this.size + " Elemente)");
        }
        else
        {
            this.indexLastElement++;
            this.stackArray[this.indexLastElement] = zahl;
        }
    }

    public int pop() throws RuntimeException
    {
        if (!this.isEmpty())
        {
            int element = this.stackArray[this.indexLastElement];
            indexLastElement--;
            return element;
        }
        else
        {
            throw new RuntimeException("Stack ist bereits leer!");
        }
    }

    public int peek() throws RuntimeException
    {
        if (!this.isEmpty())
        {
            int element = this.stackArray[this.indexLastElement];
            return element;
        }
        else
        {
            throw new RuntimeException("Stack ist leer!");
        }
    }

    public boolean isEmpty()
    {
        return this.indexLastElement < 0;
    }

    public int countItemsOnStack()
    {
        return this.indexLastElement + 1;
    }
}
