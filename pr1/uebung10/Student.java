package pr1.uebung10;

// Aufgabe 3

public class Student extends Person
{
    private int matrikelnummer;
    private String studiengang;

    public Student()
    {
        super();
    }

    public Student(String name, String vorname, int matrikelnummer, String studiengang)
    {
        super(name, vorname);
        this.matrikelnummer = matrikelnummer;
        this.studiengang = studiengang;
    }

    public int getMatrikelnummer()
    {
        return matrikelnummer;
    }

    public void setMatrikelnummer(int matrikelnummer)
    {
        this.matrikelnummer = matrikelnummer;
    }

    public String getStudiengang()
    {
        return studiengang;
    }

    public void setStudiengang(String studiengang)
    {
        this.studiengang = studiengang;
    }
}
