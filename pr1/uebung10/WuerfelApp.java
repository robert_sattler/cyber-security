package pr1.uebung10;

// Aufgabe 4

public class WuerfelApp
{
    public static void main(String[] args)
    {
        Wuerfelbecher wb = new Wuerfelbecher();

        for (int i = 1; i <= 3; i++)
        {
            System.out.println("Wurf " + i + ":");
            wb.wuerfeln();
            System.out.println();
        }
    }
}
