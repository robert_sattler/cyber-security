package pr1.uebung10;

// Aufgabe 1

public class PiEstimate
{
    public static void main(String[] args)
    {
        System.out.println(estimatePi(1000000));
    }

    public static double estimatePi(int wuerfe)
    {
        int treffer = 0;

        for (int i = 1; i <= wuerfe; i++)
        {
            double x = Math.random();
            double y = Math.random();

            if (coordIsInCircle(x, y))
            {
                treffer++;
            }
        }

        double verhaeltnis = (double) treffer / (double) wuerfe;
        double pi = verhaeltnis * 4.0;

        return pi;
    }

    public static boolean coordIsInCircle(double x, double y)
    {
        boolean isInCircle = false;

        double c = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));

        if (c <= 1.0)
        {
            isInCircle = true;
        }

        return isInCircle;
    }
}
