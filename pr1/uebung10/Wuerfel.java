package pr1.uebung10;

// Aufgabe 4

public class Wuerfel
{
    private int augenzahl;

    public Wuerfel()
    {

    }

    public Wuerfel(int augenzahl)
    {
        this.augenzahl = augenzahl;
    }

    public int getAugenzahl()
    {
        return augenzahl;
    }

    public void setAugenzahl(int augenzahl)
    {
        this.augenzahl = augenzahl;
    }
}
