package pr1.uebung10;

// Aufgabe 3

public class Professor extends Person
{
    private String fachgebiet;
    private String buero;

    public Professor()
    {
        super();
    }

    public Professor(String name, String vorname, String fachgebiet, String buero)
    {
        super(name, vorname);
        this.fachgebiet = fachgebiet;
        this.buero = buero;
    }

    public String getFachgebiet()
    {
        return fachgebiet;
    }

    public void setFachgebiet(String fachgebiet)
    {
        this.fachgebiet = fachgebiet;
    }

    public String getBuero()
    {
        return buero;
    }

    public void setBuero(String buero)
    {
        this.buero = buero;
    }
}
