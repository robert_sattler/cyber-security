package pr1.uebung10;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// Aufgabe 2

class StackTest
{

    @Test
    void push()
    {
        Stack stack = new Stack(3);
        stack.push(3);
        assertEquals(3, stack.peek());
        stack.push(8);
        assertEquals(8, stack.peek());
        stack.push(1);
        assertEquals(1, stack.peek());
        assertThrows(RuntimeException.class, () ->
        {
            stack.push(13);
        });
    }

    @Test
    void pop()
    {
        Stack stack = new Stack(3);
        stack.push(104);
        stack.push(70);
        stack.push(7);
        assertEquals(7, stack.pop());
        assertEquals(70, stack.pop());
        assertEquals(104, stack.pop());
        assertThrows(RuntimeException.class, () ->
        {
            stack.pop();
        });
    }

    @Test
    void peek()
    {
        Stack stack = new Stack(3);
        stack.push(24);
        stack.push(402);
        stack.push(32);
        assertEquals(32, stack.peek());
        assertEquals(32, stack.peek());
    }

    @Test
    void isEmpty()
    {
        Stack stack = new Stack(3);
        assertTrue(stack.isEmpty());
        stack.push(24);
        stack.push(402);
        stack.push(32);
        assertFalse(stack.isEmpty());
        stack.pop();
        assertFalse(stack.isEmpty());
        stack.pop();
        assertFalse(stack.isEmpty());
        stack.pop();
        assertTrue(stack.isEmpty());
    }

    @Test
    void countItemsOnStack()
    {
        Stack stack = new Stack(3);
        assertEquals(0, stack.countItemsOnStack());
        stack.push(24);
        assertEquals(1, stack.countItemsOnStack());
        stack.push(402);
        assertEquals(2, stack.countItemsOnStack());
        stack.push(32);
        assertEquals(3, stack.countItemsOnStack());
        stack.pop();
        assertEquals(2, stack.countItemsOnStack());
        stack.pop();
        assertEquals(1, stack.countItemsOnStack());
        stack.pop();
        assertEquals(0, stack.countItemsOnStack());
    }
}