package pr1.uebung10;

import java.util.ArrayList;
import java.util.Random;

// Aufgabe 4

public class Wuerfelbecher
{
    private ArrayList<Wuerfel> wuerfelImBecher;

    public Wuerfelbecher()
    {
        this.wuerfelImBecher = new ArrayList<>();
        this.fuelleBecher(5, 6);
    }

    public void fuelleBecher(int anzahlWuerfel, int augenzahlProWuerfel)
    {
        for (int i = 1; i <= anzahlWuerfel; i++)
        {
            this.wuerfelImBecher.add(new Wuerfel(augenzahlProWuerfel));
        }
    }

    public void wuerfeln()
    {
        int[] ergebnisListen = new int[this.wuerfelImBecher.get(0).getAugenzahl()];

        for (int i = 0; i < this.wuerfelImBecher.size(); i++)
        {
            Random r = new Random();

            int ergebnis = r.nextInt((this.wuerfelImBecher.get(i).getAugenzahl() - 1) + 1) + 1;
            ergebnisListen[ergebnis - 1] = ergebnisListen[ergebnis - 1] + 1;

            System.out.println("Würfel " + (i + 1) + ": " + ergebnis);
        }

        boolean gleicheAugenzahlGefunden = false;

        for (int i = 0; i < ergebnisListen.length; i++)
        {
            if (ergebnisListen[i] > 1)
            {
                gleicheAugenzahlGefunden = true;
                System.out.println(ergebnisListen[i] + "er Pasch für Augenzahl " + (i + 1) + ".");
            }
        }

        if (!gleicheAugenzahlGefunden)
        {
            System.out.println("Kein Würfel hat die gleiche Augenzahl.");
        }
    }
}
