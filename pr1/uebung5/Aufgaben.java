package pr1.uebung5;

public class Aufgaben
{

    public static void main(String[] args)
    {
        int[] test = {22, 5, 2, 9};

        System.out.println(zieheAbVonErster(test));
    }

    public static int zieheAbVonErster(int[] x)
    {
        int ergebnis = -999;

        if (x.length > 0)
        {
            ergebnis = x[0];

            for (int i = 1; i < x.length; i++)
            {
                ergebnis = ergebnis - x[i];
            }
        }
        else
        {
            System.out.println("Fehlerhafte Eingabe. Das Array muss mindestens ein Element beinhalten.");
        }

        return ergebnis;
    }
}
