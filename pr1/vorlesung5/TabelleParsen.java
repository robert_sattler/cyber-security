package pr1.vorlesung5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TabelleParsen
{

    @SuppressWarnings("unused")
    public static void main(String[] args) throws FileNotFoundException
    {
        Scanner sc = new Scanner(new File("./pr1/resources/daten.csv"));

        sc.nextLine();

        while (sc.hasNextLine())
        {
            String line = sc.nextLine();

            String[] worte = line.split(";");

            String nummer = worte[0];
            String jahr = worte[1];
            String gemeindeteilSchlüssel = worte[2];
            String gemeindeteilEbene = worte[3];
            String gemeindeteilName = worte[4];
            String bevInsgesamt = worte[5];
            String bevWeiblich = worte[6];
            String bevOhneMigration = worte[7];
            String bevMitMigration = worte[8];
            String bevAuslaender = worte[9];

            System.out.println(jahr);
        }

        sc.close();
    }
}