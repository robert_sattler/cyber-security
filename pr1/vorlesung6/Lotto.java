package pr1.vorlesung6;

import java.util.ArrayList;

public class Lotto
{
    public static void main(String[] args)
    {
        ArrayList<Integer> lottozahlen = new ArrayList<Integer>();

        for (int i = 0; i < 6; i++)
        {
            int neue_zahl = 0;

            do
            {
                neue_zahl = (int) (Math.random() * 49) + 1;
            }
            while (lottozahlen.contains(neue_zahl));

            lottozahlen.add(neue_zahl);
        }

        System.out.println("Die gezogenen Lottozahlen lauten:");

        for (int i = 0; i < 6; i++)
        {
            System.out.println((i + 1) + ". " + lottozahlen.get(i));
        }
    }
}