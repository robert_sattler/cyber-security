package pr1.uebung3;

import java.util.Random;

public class UebungenAufgabe1
{
    public static void main(String[] args)
    {
        int x = 0;
        int y = 1;
        int anzahl_zeichen = 7;
        String drehsUm = "Nadine soll sich entspannen und nicht mehr lernen";

        // Ausgabe zu a)
        System.out.println("Die Summe der Zahlen von " + x + " und " + y + " = " + sumAllNumbersBetweenXandY(x, y));
        // Ausgabe zu b)
        System.out.println("Zeichenkette aus " + 7 + " zuf�lligen Buchstaben des Alphabets: " + randomString(anzahl_zeichen));
        // Ausgabe zu c)
        System.out.println("'" + drehsUm + "'" + " ist umgedreht: '" + stringUmdrehen(drehsUm) + "'");
    }

    // Aufgabe 1 a)

    public static int sumAllNumbersBetweenXandY(int x, int y)
    {
        int summe = 0;

        for (int i = x; i <= y; i++)
        {
            summe = summe + i;
        }

        return summe;
    }

    // Aufgabe 1 b)

    public static String randomString(int n)
    {
        String buchstabensalat = "";

        // Zufallsgenerator erzeugen
        Random r = new Random();

        // Buchstaben von A bis Z in der ASCII-Tabelle (Start bei 65, Ende bei 90);

        int min = 65;
        int max = 90;

        for (int i = 1; i <= n; i++)
        {
				/* Zufallszahl zwischen 65 und 90 errechnen
				   Anschlie�end wird die Zahl in ein (char), also
				   Buchstaben gecasted
				*/
            buchstabensalat = buchstabensalat + (char) (r.nextInt((max - min) + 1) + min);
        }

        return buchstabensalat;
    }

    public static String stringUmdrehen(String input)
    {
        int stringLaenge = input.length();
        String umgedrehterString = "";

        for (int i = (stringLaenge - 1); i >= 0; i--)
        {
            umgedrehterString = umgedrehterString + input.charAt(i);
        }

        return umgedrehterString;
    }
}
