package pr1.uebung3;

import java.util.Scanner;

public class RechnenObjekte
{
    public static void main(String[] args)
    {
        Parameter zahlen = new Parameter();
        Scanner sc = new Scanner(System.in);

        System.out.print("Bitte geben Sie eine Zahl ein: ");
        zahlen.p1 = Double.parseDouble(sc.nextLine());

        System.out.print("Bitte geben Sie eine 2. Zahl ein: ");
        zahlen.p2 = Double.parseDouble(sc.nextLine());

        Ergebnis erg = rechne(zahlen);

        System.out.println("Die Summe ist " + erg.summe + ", das Produkt ist: " + erg.produkt);
        sc.close();
    }

    public static Ergebnis rechne(Parameter p)
    {
        Ergebnis e = new Ergebnis();
        e.summe = p.p1 + p.p2;
        e.produkt = p.p1 * p.p2;

        return e;
    }
}