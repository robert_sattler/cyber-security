package pr1.uebung3;

import java.time.LocalDateTime;

public class Kunde
{
    private static int zaehler = 1;
    public int kundennummer;
    public String vorname;
    public String name;
    public int geburtsjahr;

    public Kunde()
    {
        this.kundennummer = zaehler;
        Kunde.zaehler++;
    }

    public static void main(String[] args)
    {
        Kunde k1 = new Kunde();
        k1.vorname = "Max";
        k1.name = "Mustermann";
        k1.geburtsjahr = 1974;

        Kunde k2 = new Kunde();
        k2.vorname = "Lieschen";
        k2.name = "Müller";
        k2.geburtsjahr = 1985;

        Kunde k3 = new Kunde();
        k3.vorname = "John";
        k3.name = "Doe";
        k3.geburtsjahr = 1993;

        System.out.println("Kunde 1:");
        System.out.println(k1.toString());
        System.out.println("Alter: " + k1.getAlter() + "\n");

        System.out.println("Kunde 2:");
        System.out.println(k2.toString());
        System.out.println("Alter: " + k2.getAlter() + "\n");

        System.out.println("Kunde 3:");
        System.out.println(k3.toString());
        System.out.println("Alter: " + k3.getAlter() + "\n");
    }

    public int getAlter()
    {
        LocalDateTime aktuelleZeit = LocalDateTime.now();

        int aktuellesJahr = aktuelleZeit.getYear();
        int alter = aktuellesJahr - this.geburtsjahr;

        return alter;
    }

    public String toString()
    {
        String kundenString = "Kundennummer: " + this.kundennummer + ", Vorname: " + this.vorname + ", "
                + "Nachname: " + this.name + ", Geburtsjahr: " + this.geburtsjahr;

        return kundenString;
    }
}