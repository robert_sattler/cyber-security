package pr1.sonstiges;

import java.util.Arrays;
public class EinmalEins
{
    public static void main(String[] args)
    {
        int[][] matrix = einmalEins();

        Arrays.deepToString(matrix);

        System.out.println(".  | \t1 \t2 \t3 \t4 \t5 \t6 \t7 \t8 \t9 \t10");
        System.out.println("----------------------------------------------");

        for (int i = 0; i < 10; i++)
        {
            if (i == 9)
            {
                System.out.print((i + 1) +  " | \t");
            }
            else
            {
                System.out.print((i + 1) +  "  | \t");
            }

            for (int j = 0; j < 10; j++)
            {
                System.out.print(matrix[i][j] + "\t");
            }

            System.out.println();
        }
    }

    public static int [][] einmalEins()
    {
        int[][] einmaleins = new int[10][10];

        for (int zeile = 1; zeile <= 10; zeile++)
        {
            for (int spalte = 1; spalte <= 10; spalte++)
            {
                einmaleins[zeile - 1][spalte - 1] = zeile * spalte;
            }
        }

        return einmaleins;
    }
}