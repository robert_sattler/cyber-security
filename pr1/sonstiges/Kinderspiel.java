package pr1.sonstiges;

import java.util.Random;
import java.util.Scanner;

public class Kinderspiel
{

    public static void main(String[] args)
    {
        schereSteinPapier();
    }

    public static void schereSteinPapier()
    {

        System.out.println("Bitte wählen Sie zwischen 1 für Schere,2 für Stein,3 für Papier");
        Scanner sc = new Scanner(System.in);

        int zahl = Integer.parseInt(sc.nextLine());

        String nadineshand = "";

        if (zahl == 1)
        {
            nadineshand = "Schere";
        }
        else if (zahl == 2)
        {
            nadineshand = "Stein";
        }
        else if (zahl == 3)
        {
            nadineshand = "Papier";
        }
        else
        {
            System.out.println("Eingabe ist ungültig!");
        }

        Random r = new Random();

        int zahl_computer = r.nextInt((3 - 1) + 1) + 1;
        String hand = "";

        if (zahl_computer == 1)
        {
            hand = "Schere";
        }
        else if (zahl_computer == 2)
        {
            hand = "Stein";
        }
        else if (zahl_computer == 3)
        {
            hand = "Papier";
        }

        else
        {
            System.out.println("Eingabe ist ungültig!");
        }

        // 9 Zeilen für 9 Fälle
        // index 0 = zahl_spieler, index 1 = zahl_computer, index 2 = gewinner (0 = unentschieden, 1 = spieler, 2 = computer)
        int[][] gewinnMatrix = new int[9][3];
        gewinnMatrix[0] = new int[]{1, 1, 0};
        gewinnMatrix[1] = new int[]{1, 2, 2};
        gewinnMatrix[2] = new int[]{1, 3, 1};
        gewinnMatrix[3] = new int[]{2, 1, 1};
        gewinnMatrix[4] = new int[]{2, 2, 0};
        gewinnMatrix[5] = new int[]{2, 3, 2};
        gewinnMatrix[6] = new int[]{3, 1, 2};
        gewinnMatrix[7] = new int[]{3, 2, 1};
        gewinnMatrix[8] = new int[]{3, 3, 0};

        for (int[] zeile : gewinnMatrix)
        {
            int wahl_spieler = zeile[0];
            int wahl_computer = zeile[1];

            // 0 = Unentschieden, 1 = Spieler, 2 = Computer
            int gewinner = zeile[2];

            if (wahl_spieler == zahl && wahl_computer == zahl_computer)
            {
                switch (gewinner)
                {
                    case 0:
                        System.out.println("Unentschieden! (Ihre Wahl: " + nadineshand + ", Wahl des Computers: " + hand + ")");
                        break;
                    case 1:
                        System.out.println("Sie haben gewonnen :) (Ihre Wahl: " + nadineshand + ", Wahl des Computers: " + hand + ")");
                        break;
                    case 2:
                        System.out.println("Der Computer hat leider gewonnen :( (Ihre Wahl: " + nadineshand + ", Wahl des Computers: " + hand + ")");
                        break;
                }
            }
        }

        sc.close();
    }
}