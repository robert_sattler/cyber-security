package pr1.uebung9;

public class Bauer extends Figur
{
    public Bauer(boolean isBlack)
    {
        super(isBlack);
        this.setSymbol('\u2659');
    }
}
