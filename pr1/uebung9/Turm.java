package pr1.uebung9;

public class Turm extends Figur
{
    public Turm(boolean isBlack)
    {
        super(isBlack);
        this.setSymbol('\u2656');
    }
}
