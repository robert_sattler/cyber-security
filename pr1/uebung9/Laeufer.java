package pr1.uebung9;

public class Laeufer extends Figur
{
    public Laeufer(boolean isBlack)
    {
        super(isBlack);
        this.setSymbol('\u2657');
    }
}
