package pr1.uebung9;

public abstract class Figur
{
    protected char symbol;
    protected boolean black;

    private String typ;

    public Figur(boolean isBlack)
    {
        this.black = isBlack;
    }

    public void setSymbol(char symbol)
    {
        this.symbol = symbol;

        if (this.isBlack())
        {
            this.symbol += 6;
        }
    }

    public String getTyp()
    {
        return typ;
    }

    public char getSymbol()
    {
        return symbol;
    }

    public boolean isBlack()
    {
        return black;
    }
}
