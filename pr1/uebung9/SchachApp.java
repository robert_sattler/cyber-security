package pr1.uebung9;

import java.util.Scanner;

public class SchachApp
{

    public static void main(String[] args)
    {
        Schachbrett sb = new Schachbrett();
        Scanner sc = new Scanner(System.in);

        sb.figurenAufstellen();
        System.out.println(sb.toString());

        boolean programmBeenden = false;
        boolean weissAmZug = true;

        int zuege_weiss = 0;
        int zuege_schwarz = 0;

        while (!programmBeenden)
        {
            String spielerFarbe = "Schwarz";

            if (weissAmZug)
            {
                spielerFarbe = "Weiß";
            }

            long zeit_start = System.currentTimeMillis();

            boolean gueltigeEingabe = false;

            while (!gueltigeEingabe)
            {
                System.out.print(spielerFarbe + ", Sie sind am am Zug. Geben Sie Start und Ziel an (XX>XX)\noder beenden Sie das Programm (q): ");


                String zugString = sc.nextLine();

                if (zugString.toUpperCase().matches("[A-H][1-8]>[A-H][1-8]"))
                {
                    String aktionString = sb.ziehen(zugString, !weissAmZug);
                    String[] aktionSplit = aktionString.split("::");
                    String aktion = aktionSplit[1];

                    boolean validerZug = aktionSplit[0].equals("VALID");

                    if (validerZug)
                    {
                        gueltigeEingabe = true;

                        long zeit_ende = System.currentTimeMillis();

                        long zeit = (zeit_ende - zeit_start) / 1000;
                        //System.out.println(zeit);

                        System.out.println(sb.toString());
                        System.out.println(spielerFarbe + " " + aktion + ".\n");

                        if (weissAmZug)
                        {
                            zuege_weiss++;
                            weissAmZug = false;
                            System.out.println(spielerFarbe + " hat " + zuege_weiss + " Züge gemacht. Der letzte Zug dauerte " + zeit + "s.");
                        }
                        else
                        {
                            zuege_schwarz++;
                            weissAmZug = true;
                            System.out.println(spielerFarbe + " hat " + zuege_weiss + " Züge gemacht. Der letzte Zug dauerte " + zeit + "s.");
                        }
                    }
                    else
                    {
                        System.out.println(spielerFarbe + " " + aktion + ".\n");
                    }


                }
                else if (zugString.toLowerCase().equals("q"))
                {
                    gueltigeEingabe = true;
                    programmBeenden = true;
                }
                else
                {
                    System.out.println("Ungültige Eingabe!");
                }
            }
        }

        System.out.println("Auf Wiedersehen!");
    }
}