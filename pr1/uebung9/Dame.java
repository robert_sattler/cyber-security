package pr1.uebung9;

public class Dame extends Figur
{
    public Dame(boolean isBlack)
    {
        super(isBlack);
        this.setSymbol('\u2655');
    }
}
