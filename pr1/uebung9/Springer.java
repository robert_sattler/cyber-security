package pr1.uebung9;

public class Springer extends Figur
{
    public Springer(boolean isBlack)
    {
        super(isBlack);
        this.setSymbol('\u2658');
    }
}
