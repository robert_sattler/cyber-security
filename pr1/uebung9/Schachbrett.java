package pr1.uebung9;

public class Schachbrett
{
    private int breite = 8;
    private int höhe = 8;

    private Feld[][] felder = new Feld[höhe][breite];

    public Schachbrett()
    {
        for (int i = 0; i < felder.length; i++)
        {
            for (int j = 0; j < felder[0].length; j++)
            {
                if (i % 2 == 0 && j % 2 == 0)
                {
                    felder[i][j] = new Feld(false);
                }
                else if (i % 2 == 1 && j % 2 == 0)
                {
                    felder[i][j] = new Feld(true);
                }
                else if (i % 2 == 0 && j % 2 == 1)
                {
                    felder[i][j] = new Feld(true);
                }
                else if (i % 2 == 1 && j % 2 == 1)
                {
                    felder[i][j] = new Feld(false);
                }
            }
        } // for i
    }

    public String ziehen(String zug, boolean playerIsBlack)
    {
        int[] koordinaten = this.getChoords(zug);

        int spalte_start = koordinaten[0];
        int zeile_start = koordinaten[1];
        int spalte_ende = koordinaten[2];
        int zeile_ende = koordinaten[3];

        Feld feld_start = null;
        Feld feld_ende = null;

        for (int i = 0; i < felder.length; i++)
        {
            for (int j = 0; j < felder[0].length; j++)
            {
                if (i == zeile_start && j == spalte_start)
                {
                    feld_start = this.felder[i][j];
                }
                else if (i == zeile_ende && j == spalte_ende)
                {
                    feld_ende = this.felder[i][j];
                }
            }
        } // for i

        String[] zugSplit = zug.toUpperCase().split(">");

        String aktion = "";

        if (feld_ende.getFigur() == null && feld_start.getFigur() != null && ((feld_start.getFigur().isBlack() && playerIsBlack) || (!feld_start.getFigur().isBlack() && !playerIsBlack)))
        {
            String typ = feld_start.getFigur().getTyp();
            feld_ende.setzeFigur(feld_start.entferneFigur());
            aktion = "VALID::bewegt " + typ + " von " + zugSplit[0] + " nach " + zugSplit[1];
        }
        else
        {
            aktion = "INVALID::versucht den Zug " + zugSplit[0] + " nach " + zugSplit[1] + ". NICHT MÖGLICH!";
        }

        return aktion;
    }

    public void figurenAufstellen()
    {
        for (int i = 0; i < felder.length; i++)
        {
            if (i == 0 || i == 7)
            {
                felder[0][i].setzeFigur(new Turm(true));
                felder[7][i].setzeFigur(new Turm(false));
            }
            else if (i == 1 || i == 6)
            {
                felder[0][i].setzeFigur(new Springer(true));
                felder[7][i].setzeFigur(new Springer(false));
            }
            else if (i == 2 || i == 5)
            {
                felder[0][i].setzeFigur(new Laeufer(true));
                felder[7][i].setzeFigur(new Laeufer(false));
            }
            else if (i == 4)
            {
                felder[0][i].setzeFigur(new Koenig(true));
                felder[7][i].setzeFigur(new Koenig(false));
            }
            else if (i == 3)
            {
                felder[0][i].setzeFigur(new Dame(true));
                felder[7][i].setzeFigur(new Dame(false));
            }

            felder[1][i].setzeFigur(new Bauer(true));
            felder[6][i].setzeFigur(new Bauer(false));
        }

        // TODO: s. Aufgabe a)
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        // obere Linie
        sb.append(" \u250f");
        for (int i = 0; i < felder.length * 2 - 4; i++)
        {
            sb.append("\u2501\u2501");
        }
        sb.append("\u2513\n");

        // Felder
        for (int i = 0; i < felder.length; i++)
        {
            for (int z = 0; z < 3; z++)
            {

                // Zeilennummer
                if (z == 1)
                {
                    sb.append(8 - i);
                }
                else
                {
                    sb.append(" ");
                }

                sb.append("\u2503");
                for (int j = 0; j < felder[0].length; j++)
                {
                    sb.append(felder[i][j].getZeile(z));
                }
                sb.append("\u2503\n");
            }
        }

        // untere Linie
        sb.append(" \u2517");
        for (int i = 0; i < felder.length * 3 - 1; i++)
        {
            sb.append("\u2501");
        }
        sb.append("\u2501\u251b\n  ");

        // Buchstaben unten
        for (int i = 0; i < felder.length; i++)
        {
            sb.append(" " + (char) (65 + i) + " ");
        }

        return sb.toString();
    }

    public int[] getChoords(String eingabeZug)
    {
        int spalte_start = 0;
        int zeile_start = 0;
        int spalte_ende = 0;
        int zeile_ende = 0;

        String[] zugSplit = eingabeZug.toUpperCase().split(">");

        for (int i = 0; i < 2; i++)
        {
            int spalte = 0;
            int zeile = 0;

            switch (zugSplit[i].charAt(0))
            {
                case 'A':
                    spalte = 0;
                    break;

                case 'B':
                    spalte = 1;
                    break;

                case 'C':
                    spalte = 2;
                    break;

                case 'D':
                    spalte = 3;
                    break;

                case 'E':
                    spalte = 4;
                    break;

                case 'F':
                    spalte = 5;
                    break;

                case 'G':
                    spalte = 6;
                    break;

                case 'H':
                    spalte = 7;
                    break;
            }

            zeile = Integer.parseInt(Character.toString(zugSplit[i].charAt(1))) - 1;

            if (i == 0)
            {
                zeile_start = 7 - zeile;
                spalte_start = spalte;
            }
            else
            {
                zeile_ende = 7 - zeile;
                spalte_ende = spalte;
            }
        }

        int[] choords = {spalte_start, zeile_start, spalte_ende, zeile_ende};

        return choords;
    }
}
