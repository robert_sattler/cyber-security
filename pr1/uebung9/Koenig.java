package pr1.uebung9;

public class Koenig extends Figur
{
    public Koenig(boolean isBlack)
    {
        super(isBlack);
        this.setSymbol('\u2654');
    }
}
