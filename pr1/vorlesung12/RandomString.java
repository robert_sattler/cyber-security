package pr1.vorlesung12;

public class RandomString
{

    public static void main(String[] args)
    {
        System.out.println(randomStringArray(5));
    }

    public static String randomString(int n)
    {
        String returnString = "";

        for (int i = 1; i <= n; i++)
        {
            int randomChar = (int) ((Math.random() * ((90 - 65) + 1)) + 65);
            returnString += (char) randomChar;
        }

        return returnString;
    }

    public static char[] randomStringArray(int n)
    {
        char[] returnArray = new char[n];

        for (int i = 0; i < n; i++)
        {
            int randomChar = (int) ((Math.random() * ((90 - 65) + 1)) + 65);
            returnArray[i] = (char) randomChar;
        }

        return returnArray;
    }
}
