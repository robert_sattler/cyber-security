package blackjack;

import java.util.ArrayList;

public interface UI
{

    void startUI();

    void showMenu();

    void eingabeSpielername(Spieler spieler);

    boolean frageNachWeitererKarte(Spieler spieler);

    ArrayList<Spieler> createPlayers();

    double eingabeEinsatzVonSpieler(Spieler spieler);

    void zeigeSpielerhand(Spieler spieler, boolean alleSpielerBedient);

    SpielLogik getSpielLogik();

    void setSpielLogik(SpielLogik spielLogik);

    void announceBlackJack(Spieler spieler);

    void announce21(Spieler spieler);

    void announceVerloren(Spieler spieler);

    void announceHold(Spieler spieler);

    void announceGewinn(Spieler spieler, int type);

    void announceRunde(int runde, int gesamtrunden);

    void announceEndRound();
}
