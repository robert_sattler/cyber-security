package blackjack;

enum Farbe
{
    KREUZ, PIK, HERZ, KARO
}

public class Spielkarte
{
    private Farbe farbe;
    private String wert;

    public Spielkarte(Farbe farbe, String wert)
    {
        this.farbe = farbe;
        this.wert = wert;
    }

    public Farbe getFarbe()
    {
        return farbe;
    }

    public void setFarbe(Farbe farbe)
    {
        this.farbe = farbe;
    }

    public String getWert()
    {
        return wert;
    }

    public void setWert(String wert)
    {
        this.wert = wert;
    }

    public boolean istAss()
    {
        return this.wert.equals("A");
    }
}
