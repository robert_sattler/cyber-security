package blackjack;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class BlackJack extends Application
{
    public static void main(String[] args)
    {
        // Meine lokale Änderung
        // Zweiter anderer Commit, der mit lokalen Änderungen in Konflikt kommen wird 
        // Beides zusammengeführt.
        /*asd
         * gdsg
         *  asdas
         *  asd
         *  Langertest Kommentar */
        

        if (args.length > 0)
        {
            if (args[0].toLowerCase().equals("-gui"))
            {
                SpielLogik sl = new SpielLogik(new UI_TextKonsole());
                sl.getGameUI().startUI();
                sl.getGameUI().showMenu();
            }
            else if (args[0].toLowerCase().equals("-console"))
            {
                SpielLogik sl = new SpielLogik(new UI_TextKonsole());
                sl.getGameUI().startUI();
                sl.getGameUI().showMenu();
            }
            else
            {
                launch();
            }
        }
        else
        {
            launch();
        }
    }

    @Override
    public void start(Stage stage) throws FileNotFoundException
    {
        stage.setTitle("Black Jack");
        stage.setResizable(false);

        Image blackJackLogo = new Image(new FileInputStream("./blackjack/resources/icon512.png"), 64, 64, true, true);
        stage.getIcons().add(blackJackLogo);

        Label labelVersionChoice = new Label("Welche Version m�chtest Du starten?");
        Button buttonTextVersion = new Button("Text / Konsole");
        Button buttonGUIVersion = new Button("Graphisch / GUI");

        buttonTextVersion.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent e)
            {
                stage.close();
                SpielLogik sl = new SpielLogik(new UI_TextKonsole());
                sl.getGameUI().startUI();
                sl.getGameUI().showMenu();
            }
        });

        ImageView blackJackLogoView = new ImageView(blackJackLogo);
        blackJackLogoView.setX(240);
        blackJackLogoView.setY(20);

        labelVersionChoice.relocate(20, 20);

        HBox hbox = new HBox();

        hbox.setAlignment(Pos.BOTTOM_LEFT);
        hbox.setSpacing(15);
        hbox.setPadding(new Insets(0, 20, 20, 0));

        hbox.relocate(20, 20);
        hbox.setFillHeight(true);

        Pane pane = new Pane();
        pane.getChildren().add(labelVersionChoice);
        hbox.getChildren().add(buttonTextVersion);
        hbox.getChildren().add(buttonGUIVersion);
        hbox.getChildren().add(blackJackLogoView);
        pane.getChildren().add(hbox);
        //stage.initModality(Modality.APPLICATION_MODAL);
        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
    }
}