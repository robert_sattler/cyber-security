package blackjack;

import java.util.ArrayList;
import java.util.Scanner;

public class UI_TextKonsole implements UI
{

    private SpielLogik spielLogik;
    private Scanner sc;

    public void startUI()
    {
        this.sc = new Scanner(System.in);
        System.out.println("Hallo! Willkommen zu unserem Black Jack Spiel!");
        System.out.println("----------------------------------------------\n");
    }

    public void showMenu()
    {
        boolean programmBeenden = false;

        while (!programmBeenden)
        {
            System.out.println("Menü:");
            System.out.println("(1) Neues Spiel starten");
            System.out.println("(2) Programm beenden");
            System.out.print("Deine Wahl: ");

            int auswahl = 0;

            try
            {
                auswahl = Integer.parseInt(this.sc.nextLine());
            }
            catch (Exception e)
            {
                System.out.println("Ungültige Eingabe! Bitte gib eine Zahl ein.");
                continue;
            }

            switch (auswahl)
            {
                case 1:
                    this.spielLogik.gameLoop();
                    break;

                case 2:
                    programmBeenden = true;
                    break;

                default:
                    System.out.println("Wähle zwischen 1 und 2!");
                    break;
            }
        }

        System.out.println("Auf Wiedersehen!");
        this.sc.close();
        System.exit(0);
    }

    public void eingabeSpielername(Spieler spieler)
    {
        System.out.print("\nGib Deinen Namen ein: ");
        String name = this.sc.nextLine();
        spieler.setName(name);
        System.out.println("\nHallo " + spieler.getName() + ", dein Spiel beginnt, viel Glück!");
    }

    public void zeigeSpielerhand(Spieler spieler, boolean alleSpielerBedient)
    {
        String spielerhandString = "";

        for (Spielkarte karte : spieler.getKartenHandGesamt())
        {
            if (spielerhandString.length() == 0)
            {
                spielerhandString = karte.getWert();
            }
            else
            {
                if (spieler.isCroupier() && !alleSpielerBedient)
                {
                    spielerhandString += ", [?]";
                }
                else
                {
                    spielerhandString += ", " + karte.getWert();
                }
            }
        }

        if (spieler.isCroupier())
        {
            if (!alleSpielerBedient)
            {
                System.out.println("\nDer Croupier hat: " + spielerhandString + " (Gesamtwert: ?)");
            }
            else
            {
                System.out.println("\nDer Croupier hat: " + spielerhandString + " (Gesamtwert: " + this.spielLogik.ermittleWertSpielerhand(spieler) + ")");
            }
        }
        else
        {
            System.out.println("\nHallo " + spieler.getName() + ", Deine aktuelle Hand ist: " + spielerhandString + " (Gesamtwert: " + this.spielLogik.ermittleWertSpielerhand(spieler) + ")");
        }
    }

    public boolean frageNachWeitererKarte(Spieler spieler)
    {
        boolean weitermachen = false;
        boolean eingabeGueltig = false;

        while (!eingabeGueltig)
        {
            System.out.print("\nMöchtest Du eine weitere Karte (j/n)? ");
            String eingabe = this.sc.nextLine();

            if (eingabe.toLowerCase().equals("j"))
            {
                eingabeGueltig = true;
                weitermachen = true;
            }
            else if (eingabe.toLowerCase().equals("n"))
            {
                eingabeGueltig = true;
                weitermachen = false;
            }
            else
            {
                System.out.println("\nUngültige Eingabe! Bitte gib \"j\" oder \"n\" ein.\n");
                eingabeGueltig = false;
            }
        }

        return weitermachen;
    }

    public ArrayList<Spieler> createPlayers()
    {
        ArrayList<Spieler> spielerListe = new ArrayList<Spieler>();

        boolean eingabeGueltig = false;

        while (!eingabeGueltig)
        {
            System.out.print("Anzahl der teilnehmenden Spieler (1 - 8): ");

            int anzahl = 0;

            try
            {
                anzahl = Integer.parseInt(this.sc.nextLine());
            }
            catch (Exception e)
            {
                eingabeGueltig = false;
                System.out.println("Ungültige Eingabe! Bitte gib eine Zahl ein.");
                continue;
            }

            if (anzahl > 0 && anzahl <= 8)
            {
                eingabeGueltig = true;

                for (int i = 1; i <= anzahl; i++)
                {
                    Spieler spieler = new Spieler();
                    this.eingabeSpielername(spieler);
                    spielerListe.add(spieler);
                }
            }
            else
            {
                eingabeGueltig = false;
                System.out.println("Die Spieleranzahl muss zwischen 1 und 8 liegen!");
            }
        }

        return spielerListe;
    }

    public double eingabeEinsatzVonSpieler(Spieler spieler)
    {
        boolean eingabeGueltig = false;
        double einsatz = 0.0;

        while (!eingabeGueltig)
        {
            System.out.print("\n" + spieler.getName() + ", Dein aktueller Kontostand: " + String.format("%.2f", spieler.getKontostand()) +
                    " €\nBitte wähle Deinen Einsatz (5 € bis 100 €): ");

            try
            {
                einsatz = Double.parseDouble(this.sc.nextLine());

                if (einsatz >= 5.0 && einsatz <= 100.0)
                {
                    if (!(einsatz <= spieler.getKontostand()))
                    {
                        eingabeGueltig = false;
                        System.out.println("\nDer Einsatz übersteigt Deinen Kontostand!\n");
                    }
                    else
                    {
                        eingabeGueltig = true;
                    }
                }
                else
                {
                    eingabeGueltig = false;
                    System.out.println("\nDer Einsatz muss zwischen 5 € und 100 € liegen!\n");
                }
            }
            catch (Exception e)
            {
                eingabeGueltig = false;
                System.out.println("\nUngültige Eingabe! Bitte gib eine Zahl ein.\n");
            }
        }

        return einsatz;
    }

    public void announceRunde(int runde, int gesamtrunden)
    {
        System.out.println("\nRunde " + runde + "/" + gesamtrunden + "!");
    }

    public void announceBlackJack(Spieler spieler)
    {
        System.out.println("\nBlack Jack für Dich, " + spieler.getName() + " - Du hältst automatisch!");
    }

    public void announce21(Spieler spieler)
    {
        System.out.println("\nDer Wert Deines Blatts ist 21, " + spieler.getName() + " - Du hältst automatisch!");
    }

    public void announceVerloren(Spieler spieler)
    {
        System.out.println("\nTut mir Leid " + spieler.getName() + ", Du hast leider verloren.");
    }

    public void announceHold(Spieler spieler)
    {
        System.out.println("\nAlles klar, " + spieler.getName() + " Du hältst bei " + this.spielLogik.ermittleWertSpielerhand(spieler) + ".");
    }

    public void announceGewinn(Spieler spieler, int type)
    {

        String spielerName = spieler.getName();
        if (Character.toLowerCase(spielerName.charAt(spielerName.length() - 1)) == 's' ||
                Character.toLowerCase(spielerName.charAt(spielerName.length() - 1)) == 'x' ||
                Character.toLowerCase(spielerName.charAt(spielerName.length() - 1)) == 'z')
        {
            spielerName += "'";
        }
        else
        {
            spielerName += "s";
        }

        switch (type)
        {
            case 0:
                System.out.println(spielerName + " neuer Kontostand: " + String.format("%.2f", spieler.getKontostand()) + " € (" + spieler.getEinsatz() + " € verloren)");
                break;

            case 1:
                System.out.println(spielerName + " neuer Kontostand: " + String.format("%.2f", spieler.getKontostand()) + " € (Einsatz zurückerhalten)");
                break;

            case 2:
                System.out.println(spielerName + " neuer Kontostand: " + String.format("%.2f", spieler.getKontostand()) + " € (" + spieler.getEinsatz() + " € gewonnen)");
                break;

            case 3:
                System.out.println(spielerName + " neuer Kontostand: " + String.format("%.2f", spieler.getKontostand()) + " € (" + (spieler.getEinsatz() * 1.5) + " € gewonnen)");
                break;
        }
    }

    public void announceEndRound()
    {
        System.out.println("Die Runde ist beendet. Es folgen die Ergebnisse: \n");
    }

    public SpielLogik getSpielLogik()
    {
        return spielLogik;
    }

    public void setSpielLogik(SpielLogik spielLogik)
    {
        this.spielLogik = spielLogik;
    }
}
