package blackjack;

import java.util.ArrayList;
import java.util.Collections;

public class SpielLogik
{

    private UI gameUI;
    private ArrayList<Spieler> spielerListe;
    private ArrayList<Spielkarte> kartenDeck;
    private int runden;

    public SpielLogik()
    {
        this.runden = 3;
        this.spielerListe = new ArrayList<Spieler>();
    }

    public SpielLogik(UI ui)
    {
        this();
        this.gameUI = ui;
        this.gameUI.setSpielLogik(this);
    }

    public int getRunden()
    {
        return runden;
    }

    public void setRunden(int runden)
    {
        this.runden = runden;
    }

    public UI getGameUI()
    {
        return gameUI;
    }

    public void setGameUI(UI ui)
    {
        this.gameUI = ui;
        this.gameUI.setSpielLogik(this);
    }

    public void gameLoop()
    {
        /*
         * Spielbeginn
         */

        // Kartendeck (neu) erzeugen
        this.erstelleKartenDeck();

        // Croupier anlegen
        Spieler croupier = new Spieler("Croupier");
        croupier.setCroupier(true);

        this.spielerListe = this.gameUI.createPlayers();

        int aktuelleRunde = 1;

        // Solange wir noch nicht die letzte Runde überschritten haben
        while (aktuelleRunde <= this.runden)
        {

            /*
             * Die Runde beginnt
             */

            this.gameUI.announceRunde(aktuelleRunde, this.runden);

            // Karten mischen vor jeder Runde
            this.mischeKarten();

            croupier.resetRunde();

            // Frage Einsatz ab für jeden Spieler und ziehe die erste Karte
            for (Spieler spieler : this.spielerListe)
            {
                spieler.resetRunde();
                double einsatz = this.gameUI.eingabeEinsatzVonSpieler(spieler);
                spieler.setEinsatz(einsatz);
                this.zieheKarte(spieler);
            }

            // Croupier zieht eine Karte
            this.zieheKarte(croupier);

            // Ziehe die zweite Karte der Spieler
            for (Spieler spieler : this.spielerListe)
            {
                this.zieheKarte(spieler);
            }

            // Croupier zieht zweite (verdeckte) Karte
            this.zieheKarte(croupier);

            for (Spieler spieler : this.spielerListe)
            {
                this.gameUI.zeigeSpielerhand(spieler, false);
                this.gameUI.zeigeSpielerhand(croupier, false);

                while (!spieler.istGleich21() && !spieler.istUeber21() && gameUI.frageNachWeitererKarte(spieler))
                {
                    this.zieheKarte(spieler);
                    this.ermittleWertSpielerhand(spieler);
                    this.gameUI.zeigeSpielerhand(spieler, false);
                    this.gameUI.zeigeSpielerhand(croupier, false);
                }

                if (spieler.hatBlackJack())
                {
                    this.gameUI.announceBlackJack(spieler);
                }
                else if (spieler.istGleich21())
                {
                    this.gameUI.announce21(spieler);
                }
                else if (spieler.istUeber21())
                {
                    this.gameUI.announceVerloren(spieler);
                }
                else
                {
                    this.gameUI.announceHold(spieler);
                }
            }

            this.gameUI.zeigeSpielerhand(croupier, true);

            // Wenn das Blatt des Croupiers nicht mindestens den Wert 17 hat, muss er weitere Karten ziehen..
            while (this.ermittleWertSpielerhand(croupier) < 17)
            {
                this.zieheKarte(croupier);
                this.gameUI.zeigeSpielerhand(croupier, true);
            }

            this.gameUI.announceEndRound();

            // Gewinnermittlung
            for (Spieler spieler : this.spielerListe)
            {
                // Croupier hat überreizt
                if (this.ermittleWertSpielerhand(croupier) > 21)
                {
                    if (!spieler.istUeber21() && spieler.hatBlackJack())
                    {
                        spieler.setKontostand(spieler.getKontostand() + (spieler.getEinsatz() * 1.5));
                        this.gameUI.announceGewinn(spieler, 3);
                    }
                    else if (!spieler.istUeber21())
                    {
                        spieler.setKontostand(spieler.getKontostand() + spieler.getEinsatz());
                        this.gameUI.announceGewinn(spieler, 2);
                    }
                    else
                    {
                        spieler.setKontostand(spieler.getKontostand() - spieler.getEinsatz());
                        this.gameUI.announceGewinn(spieler, 0);
                    }
                }
                else
                {
                    if (!spieler.istUeber21() && spieler.hatBlackJack() && croupier.hatBlackJack())
                    {
                        this.gameUI.announceGewinn(spieler, 1);
                    }
                    else if (!spieler.istUeber21() && spieler.hatBlackJack() && !croupier.hatBlackJack())
                    {
                        spieler.setKontostand(spieler.getKontostand() + (spieler.getEinsatz() * 1.5));
                        this.gameUI.announceGewinn(spieler, 3);
                    }
                    else if (!spieler.istUeber21() && !croupier.hatBlackJack() && (this.ermittleWertSpielerhand(spieler) == this.ermittleWertSpielerhand(croupier)))
                    {
                        this.gameUI.announceGewinn(spieler, 1);
                    }
                    else if (!spieler.istUeber21() && (this.ermittleWertSpielerhand(spieler) > this.ermittleWertSpielerhand(croupier)))
                    {
                        spieler.setKontostand(spieler.getKontostand() + spieler.getEinsatz());
                        this.gameUI.announceGewinn(spieler, 2);
                    }
                    else
                    {
                        spieler.setKontostand(spieler.getKontostand() - spieler.getEinsatz());
                        this.gameUI.announceGewinn(spieler, 0);
                    }
                }
            }

            aktuelleRunde++;
        }
    }

    public int ermittleWertSpielerhand(Spieler spieler)
    {
        int kartenwert = 0;

        // Normale Spieler
        if (!spieler.isCroupier())
        {
            for (Spielkarte karte : spieler.getKartenHandNormal())
            {
                if (karte.getWert().equals("J") || karte.getWert().equals("Q") || karte.getWert().equals("K"))
                {
                    kartenwert = kartenwert + 10;
                }
                else
                {
                    kartenwert = kartenwert + Integer.parseInt(karte.getWert());
                }

                if (kartenwert == 21)
                {
                    spieler.setGleich21(true);
                }
                else if (kartenwert > 21)
                {
                    spieler.setUeber21(true);
                }
            }

            for (@SuppressWarnings("unused") Spielkarte karte : spieler.getKartenHandAsse())
            {
                if ((kartenwert + 11) <= 21)
                {
                    if ((kartenwert + 11) == 21)
                    {
                        spieler.setGleich21(true);
                    }

                    kartenwert = kartenwert + 11;
                }
                else if ((kartenwert + 11) > 21)
                {

                    if (kartenwert + 1 > 21)
                    {
                        spieler.setGleich21(false);
                        spieler.setUeber21(true);
                    }
                    else if (kartenwert + 1 == 21)
                    {
                        spieler.setGleich21(true);
                    }

                    kartenwert = kartenwert + 1;
                }
            }
        }
        // Croupier
        else
        {
            for (Spielkarte karte : spieler.getKartenHandGesamt())
            {
                if (karte.getWert().equals("J") || karte.getWert().equals("Q") || karte.getWert().equals("K"))
                {
                    kartenwert = kartenwert + 10;
                }
                else if (karte.istAss())
                {
                    if ((kartenwert + 11) <= 21)
                    {
                        kartenwert = kartenwert + 11;
                    }
                    else
                    {
                        kartenwert = kartenwert + 1;
                    }
                }
                else
                {
                    kartenwert = kartenwert + Integer.parseInt(karte.getWert());
                }
            }

            if (kartenwert > 21)
            {
                spieler.setUeber21(true);
            }
            else if (kartenwert == 21)
            {
                spieler.setGleich21(true);
            }
        }

        if (spieler.istGleich21() && spieler.getKartenHandGesamt().size() == 2)
        {
            spieler.setBlackJack(true);
        }

        return kartenwert;
    }

    public void zieheKarte(Spieler spieler)
    {
        Spielkarte gezogeneKarte = this.kartenDeck.get(0);
        this.kartenDeck.remove(0);
        this.kartenDeck.add(gezogeneKarte);

        if (gezogeneKarte.istAss())
        {
            spieler.getKartenHandAsse().add(gezogeneKarte);
        }
        else
        {
            spieler.getKartenHandNormal().add(gezogeneKarte);
        }

        spieler.getKartenHandGesamt().add(gezogeneKarte);
    }

    public void mischeKarten()
    {
        // Verteilt alle Elemente innerhalb der ArrayList (Kartendeck) zufällig
        Collections.shuffle(this.kartenDeck);
    }

    public void erstelleKartenDeck()
    {
        this.kartenDeck = new ArrayList<Spielkarte>();

        // Schleife für 6 Decks (Das Gesamtdeck besteht aus 6 Decks zu je 52 Karten)
        for (int j = 1; j <= 6; j++)
        {
            // 4x Schleife (für alle 4 Farben)
            for (int i = 1; i <= 4; i++)
            {
                // Variable für die Farbe initialisieren
                Farbe farbe = null;

                // Farbe festlegen (je nach i - Kreuz, Pik, Herz, Karo)
                switch (i)
                {
                    case 1:
                        farbe = Farbe.KREUZ;
                        break;

                    case 2:
                        farbe = Farbe.PIK;
                        break;

                    case 3:
                        farbe = Farbe.HERZ;
                        break;

                    case 4:
                        farbe = Farbe.KARO;
                        break;
                }

                // Schleife für die einzelnen Kartenwerte
                for (int k = 2; k <= 14; k++)
                {
                    String wert = null;

                    // Wenn Zähler <= 10, dann nimm die Zahl und wandle Sie in einen String um
                    // Falls k mindestens 11, dann weise die jeweiligen Bildwerte (J, Q, K, Ass) zu

                    if (k <= 10)
                    {
                        wert = Integer.toString(k);
                    }
                    else
                    {
                        switch (k)
                        {
                            case 11:
                                wert = "J";
                                break;

                            case 12:
                                wert = "Q";
                                break;

                            case 13:
                                wert = "K";
                                break;

                            case 14:
                                wert = "A";
                                break;
                        }
                    }

                    // Füge die Karte dem Kartendeck hinzu
                    this.kartenDeck.add(new Spielkarte(farbe, wert));
                }
            }
        }
    }
}
