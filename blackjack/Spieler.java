package blackjack;

import java.util.ArrayList;

public class Spieler
{
    private String name;
    private double kontostand;
    private double einsatz;
    private ArrayList<Spielkarte> kartenHandNormal;
    private ArrayList<Spielkarte> kartenHandAsse;
    private ArrayList<Spielkarte> kartenHandGesamt;
    private boolean blackJack;
    private boolean ueber21;
    private boolean gleich21;
    private boolean isCroupier;

    public Spieler()
    {
        this.resetSpieler();
    }

    public Spieler(String name)
    {
        this();
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public double getKontostand()
    {
        return kontostand;
    }

    public void setKontostand(double kontostand)
    {
        this.kontostand = kontostand;
    }

    public ArrayList<Spielkarte> getKartenHandNormal()
    {
        return kartenHandNormal;
    }

    public ArrayList<Spielkarte> getKartenHandAsse()
    {
        return kartenHandAsse;
    }

    public void resetSpieler()
    {
        this.resetRunde();
        this.setKontostand(500.0);
        this.setCroupier(false);
    }

    public void resetRunde()
    {
        this.kartenHandNormal = new ArrayList<Spielkarte>();
        this.kartenHandAsse = new ArrayList<Spielkarte>();
        this.kartenHandGesamt = new ArrayList<Spielkarte>();
        this.setEinsatz(0.0);
        this.setBlackJack(false);
        this.setUeber21(false);
        this.setGleich21(false);
    }

    public boolean hatBlackJack()
    {
        return blackJack;
    }

    public void setBlackJack(boolean blackJack)
    {
        this.blackJack = blackJack;
    }

    public boolean istUeber21()
    {
        return ueber21;
    }

    public void setUeber21(boolean ueber21)
    {
        this.ueber21 = ueber21;
    }

    public ArrayList<Spielkarte> getKartenHandGesamt()
    {
        return kartenHandGesamt;
    }

    public void setKartenHandGesamt(ArrayList<Spielkarte> kartenHandGesamt)
    {
        this.kartenHandGesamt = kartenHandGesamt;
    }

    public boolean isCroupier()
    {
        return isCroupier;
    }

    public void setCroupier(boolean isCroupier)
    {
        this.isCroupier = isCroupier;
    }

    public boolean istGleich21()
    {
        return gleich21;
    }

    public void setGleich21(boolean gleich21)
    {
        this.gleich21 = gleich21;
    }

    public double getEinsatz()
    {
        return einsatz;
    }

    public void setEinsatz(double einsatz)
    {
        this.einsatz = einsatz;
    }
}
