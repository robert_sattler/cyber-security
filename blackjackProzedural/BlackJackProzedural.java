package blackjackProzedural;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class BlackJackProzedural
{

    public static void main(String[] args)
    {

        Scanner sc = new Scanner(System.in);
        System.out.println("Hallo! Willkommen zu unserem Black Jack Spiel!");
        System.out.println("----------------------------------------------\n");
        System.out.print("Geben Sie Ihren Namen ein: ");
        String spielername = sc.nextLine();
        System.out.println("Hallo " + spielername + ", dein Spiel beginnt, viel Glück!");

        System.out.print("Möchten Sie eine weitere Karte (j/n)? ");
        String weitermachen = sc.nextLine();

        String[] kartendeck = {"2", "3", "4", "5", "6",
                "7", "8", "9", "10", "B", "D", "K", "A"};

        List<String> meineHand = new ArrayList<String>();
        List<String> meineHandAsse = new ArrayList<String>();

        boolean blackJack = false;
        boolean verloren = false;

        while (weitermachen.toLowerCase().equals("j"))
        {
            Random zufallsgenerator = new Random();

            int index = zufallsgenerator.nextInt(12 - 0 + 1) + 0;
            if (kartendeck[index].equals("A"))
            {
                meineHandAsse.add(kartendeck[index]);
            }
            else
            {
                meineHand.add(kartendeck[index]);
            }
            int kartenwert = 0;

            for (int i = 0; i < meineHand.size(); i++)
            {
                if (meineHand.get(i).equals("B") || meineHand.get(i).equals("D") || meineHand.get(i).equals("K"))
                {
                    kartenwert = kartenwert + 10;
                }
                else
                {
                    kartenwert = kartenwert + Integer.parseInt(meineHand.get(i));
                }

                if (kartenwert == 21)
                {
                    blackJack = true;
                }
                else if (kartenwert > 21)
                {
                    verloren = true;
                }
            }

            if (!(blackJack || verloren))
            {
                for (int i = 0; i < meineHandAsse.size(); i++)
                {
                    if (kartenwert + 11 < 21)
                    {
                        kartenwert = kartenwert + 11;
                    }
                    else if (kartenwert + 11 > 21)
                    {
                        kartenwert = kartenwert + 1;
                    }
                    else if (kartenwert + 11 == 21)
                    {
                        blackJack = true;
                        kartenwert = kartenwert + 11;
                    }
                }
            }

            System.out.println("Du hast die Karte " + kartendeck[index] + " gezogen, dein aktueller Spielstand beträgt: " + kartenwert);

            if (!(blackJack || verloren))
            {
                System.out.print("Möchten Sie eine weitere Karte (j/n)? ");
                weitermachen = sc.nextLine();
            }
            else
            {
                weitermachen = "n";
            }
        }

        if (blackJack)
        {
            System.out.println("BLAAAAAAAAAAAAAAAAAACKJACK! Gib mir das Geld!");
        }
        else if (verloren)
        {
            System.out.println("Ein Satz mit X, das wahr wohl nix.");
        }
        else
        {
            System.out.println("Ob ich behindert bin.");
        }

        System.out.println("Auf Wiedersehen! ");

        sc.close();
    }
}


